[%# The contents of this file are subject to the Mozilla Public
  # License Version 1.1 (the "License"); you may not use this file
  # except in compliance with the License. You may obtain a copy of
  # the License at http://www.mozilla.org/MPL/
  #
  # Software distributed under the License is distributed on an "AS
  # IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or
  # implied. See the License for the specific language governing
  # rights and limitations under the License.
  #
  # The Original Code is the Bugzilla Bug Tracking System.
  #
  # The Initial Developer of the Original Code is Frédéric Buclin.
  #
  # Contributor(s): Frédéric Buclin <LpSolit@gmail.com>
  #%]

[%# INTERFACE:
  # abuser: identity of the user who created the (invalid?) token.
  # token_action: the action the token was supposed to serve.
  # expected_action: the action the user was going to do.
  # script_name: the script generating this warning.
  # alternate_script: the suggested script to redirect the user to
  #                   if he declines submission.
  #%]

[% PROCESS "global/field-descs.none.tmpl" %]

[% PROCESS global/header.html.tmpl title = "Suspicious Action"
                                   style_urls = ['skins/standard/global.css'] %]

[% IF abuser %]
  <div class="throw_error">
    <p>Quando voce visualiza formularios adiministrativos no [% terms.Bugzilla %], um token é
    gerado aleatoriamente e armazenado no banco de dados e no formulario que voce carregou,
    para ter certeza que o seu pedido de mudança foi resultado de um pedido de submit
    gerado pelo [% terms.Bugzilla %]. Infelizmente, o token usado agora esta incorreto, 
    isso significa que voce não veio da página correta.
    O token a seguir já foi usado anteriormente:</p>

    <table border="0" cellpadding="5" cellspacing="0">
      [% IF token_action != expected_action %]
        <tr>
          <th>Action&nbsp;stored:</th>
          <td>[% token_action FILTER html %]</td>
        </tr>
        <tr>
          <th>&nbsp;</th>
          <td>
            Essa ação não coincide com a ação esperada ([% expected_action FILTER html %]).
          </td>
        </tr>
      [% END %]

      [% IF abuser != user.identity %]
        <tr>
          <th>Generated&nbsp;by:</th>
          <td>[% abuser FILTER html %]</td>
        </tr>
        <tr>
          <th>&nbsp;</th>
          <td>
            Esse token não gerado por voce. É possivel que alguem tenha tentado te enganar!
          </td>
        </tr>
      [% END %]
    </table>

    <p>Por favor reporte este problema para [%+ Param("maintainer") FILTER html %].</p>
  </div>
[% ELSE %]
  <div class="throw_error">
    Parece que você não veio da página correta(voce nao possui um token válido para a ação
     <em>[% expected_action FILTER html %]</em> enquanto processando o
    '[% script_name FILTER html%]' script). Uma das razões pode ser:<br>
    <ul>
      <li>Voce clicou no botão de "Voltar" do seu navegador depois de ter tido sucesso
      nas mudanças que enviou, o que geralmente não é uma boa ideia.</li>
      <li>Voce inseriu uma URL na sua barra de endereços diretamete, que deve ser seguro.</li>
      <li>Voce clicou em uma URL que o redirecionou para cá <b>sem seu consentimento</b>,
      e nesse caso essa ação pode sem muito mais critica.</li>
    </ul>
    Voce tem certeza que quer validar essas mudanças mesmo assim? Isso pode resultar em
    resultados inexperados e indesejados.
  </div>

  <form name="check" id="check" method="post" action="[% script_name FILTER html %]">
    [% PROCESS "global/hidden-fields.html.tmpl"
               exclude="^(Bugzilla_login|Bugzilla_password)$" %]
    <input type="submit" id="confirm" value="Confirmar Mudanças">
  </form>
  <p>Ou eliminar todas as modificações e retornar para <a href="[% alternate_script FILTER html %]">
    [%- alternate_script FILTER html %]</a>.</p>
[% END %]

[% PROCESS global/footer.html.tmpl %]
