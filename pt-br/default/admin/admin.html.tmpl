[%# The contents of this file are subject to the Mozilla Public
  # License Version 1.1 (the "License"); you may not use this file
  # except in compliance with the License. You may obtain a copy of
  # the License at http://www.mozilla.org/MPL/
  #
  # Software distributed under the License is distributed on an "AS
  # IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or
  # implied. See the License for the specific language governing
  # rights and limitations under the License.
  #
  # The Original Code is the Bugzilla Bug Tracking System.
  #
  # Contributor(s): Frédéric Buclin <LpSolit@gmail.com>
  #%]

[% PROCESS global/variables.none.tmpl %]

[% title = BLOCK %]
  Administer your installation ([% terms.Bugzilla %]
  [%+ constants.BUGZILLA_VERSION FILTER html %])
[% END %]

[% PROCESS global/header.html.tmpl title = title
                                   style_urls = ['skins/standard/admin.css']
                                   doc_section = "administration.html"
%]

<div>
  Essa página esta acessivel apenas para usuários com poderes. Você pode acessar paginas
  administrativas por aqui(baseado nos seus privilégios), permitindo a você configurar
  diferentes aspectos da sua instalação. Nota: algumas seções podem não estar acessíveis
  para você e estão marcadas usando uma cor clara.
</div>

<table>
  <tr>
    <td class="admin_links">
      <dl>
        [% class = user.in_group('tweakparams') ? "" : "forbidden" %]
        <dt id="parameters" class="[% class %]"><a href="editparams.cgi">Parametros</a></dt>
        <dd class="[% class %]">Defina parametros do core da instalação. Esse lugar
        é onde você especifica a URL para acessar sua instalação, determina como os 
        usuários irão se autenticar, escolhe quais campos do [% terms.bug %] para mostrar,
        seleciona o tipo de envio de email para enviar notificações, escolhe qual grupo
        de usuários podem usar gráficos e compartilhar queries, e muito mais.</dd>

        <dt id="preferences" class="[% class %]"><a href="editsettings.cgi">Preferencias Padrão</a></dt>
        <dd class="[% class %]">Define as preferências padrão de usuários. Esses são os valores
        que você pode setar por padrão para todos os usuários. Usuários serão capazes de editar
        suas próprias preferencias através de <a href="userprefs.cgi?tab=settings">Preferencias</a>.</dd>

        [% class = user.in_group('editcomponents') ? "" : "forbidden" %]
        <dt id="sanitycheck" class="[% class %]"><a href="sanitycheck.cgi">Verificação de Consistencia</a></dt>
        <dd class="[% class %]">Execute verificação de consistencia para localizar problemas no seu banco de dados.
        Isso pode durar alguns vários minutos dependendo do tamanho da sua instalação.
	Você pode automatizar essa checagem rodando  <tt>sanitycheck.pl</tt> na sua cron.
        Uma notificação será enviada por email a um usuário especifico no caso de erros serem detectados.</dd>

        [% class = (user.in_group('editusers') || user.can_bless) ? "" : "forbidden" %]
        <dt id="users" class="[% class %]"><a href="editusers.cgi">Usuários</a></dt>
        <dd class="[% class %]">Crie novas contas ou edite as existentes. Você pode
        também adicionar ou remover usuários de grupos.</dd>

        [% class = (Param('useclassification') && user.in_group('editclassifications')) ? "" : "forbidden" %]
        <dt id="classifications" class="[% class %]"><a href="editclassifications.cgi">Classificações</a></dt>
        <dd class="[% class %]">Se a sua instalação precisa controlar vários produtos,
        é uma boa ideia agrupá-los em categorias distintas. Isso faz com que os usuáios
        achem as informações mais facilmente fazendo buscas ou cadastrando novos [% terms.bugs %].</dd>

        [% class = (user.in_group('editcomponents')
                    || user.get_products_by_permission("editcomponents").size) ? "" : "forbidden" %]
        <dt id="products" class="[% class %]"><a href="editproducts.cgi">Produtoss</a></dt>
        <dd class="[% class %]">Edite tudo relacionado a produtos, incluindo grupos de restrições
        que ajudarão a você definir quem pode acessar [% terms.bugs %] desses produtos. Você também pode
        editar alguns atributos específicos de produtos como
        <a href="editcomponents.cgi">componentes</a>, <a href="editversions.cgi">versões</a>
        e <a href="editmilestones.cgi">marcos</a> diretamente.</dd>

        [% class = user.in_group('editcomponents') ? "" : "forbidden" %]
        <dt id="flags" class="[% class %]"><a href="editflagtypes.cgi">Flags</a></dt>
        <dd class="[% class %]">Uma flag é um atributo customizado de 4 estados de [% terms.bugs %]
        e/ou anexos. Esses estados são: granted, denied, requested and undefined.
	Você pode setar quantas flags desejar por [% terms.bug %], e definir quais usuarios
        poderão modificar.</dd>

        [% Hook.process('end_links_left') %]
      </dl>
    </td>

    <td class="admin_links">
      <dl>
        [% class = user.in_group('admin') ? "" : "forbidden" %]
        <dt id="custom_fields" class="[% class %]"><a href="editfields.cgi">Campos Customizados</a></dt>
        <dd class="[% class %]">[% terms.Bugzilla %] permite a você, definir campos que não
	são implementados por padrão, baseados na sua necessidade.
	Estes campos pode ser usados como qualquer outro campo, isso quer dizer que você pode
        setá-los no [% terms.bugs %] e aplicar qualquer busca envolvendo eles.<br>
	Antes de criar novos campos, tenha em mente que muitos campos podem tornar
	o uso da interface muito complexa e ruim de usar. Tenha certeza que já esgotou todas
	as outras possibilidades antes de fazer isso.</dd>

        <dt id="field_values" class="[% class %]"><a href="editvalues.cgi">Valores de campos</a></dt>
        <dd class="[% class %]">Defina valores para campos onde os seus valores devem pertencer a
	algum tipo de lista. Esse é também o lugar onde você define os valores de alguns tipos
	de campos personalizados.</dd>

        <dt id="status_workflow" class="[% class %]"><a href="editworkflow.cgi">[%terms.Bug %] Status Workflow</a></dt>
        <dd class="[% class %]">Customise seu workflow e escolha status [% terms.bug %] iniciais
	disponiveis na criação de [% terms.bug %] e permita transições de status de [% terms.bug %] queando estiver
	editando [% terms.bugs %] existentes.</dd>

        [% class = user.in_group('creategroups') ? "" : "forbidden" %]
        <dt id="groups" class="[% class %]"><a href="editgroups.cgi">Grupos</a></dt>
        <dd class="[% class %]">Defina os grupos que serão usados na sua instalação.
	Eles também podem ser usados para definir privilégios de novos usuários ou restringir
	o acesso a alguns [% terms.bugs %].</dd>

        [% class = user.in_group('editkeywords') ? "" : "forbidden" %]
        <dt id="keywords" class="[% class %]"><a href="editkeywords.cgi">Palavras chave</a></dt>
        <dd class="[% class %]">Defina as palavras chave que serão usadas com [% terms.bugs %]. Palavras chave
	são um modo fácil de tagear [% terms.bugs %] para poder acha-los mais facilmente depois.</dd>

        [% class = user.in_group('bz_canusewhines') ? "" : "forbidden" %]
        <dt id="whining" class="[% class %]"><a href="editwhines.cgi">Whining</a></dt>
        <dd class="[% class %]">Defina queries que serão executadas em uma data e hora especifica,
	e obtenha seus resultados diretamente por email. Esse é um bom modod de criar lembretes
	e trackear as atividades de sua instalação.</dd>

        [% Hook.process('end_links_right') %]
      </dl>
    </td>
  </tr>
</table>

[% PROCESS global/footer.html.tmpl %]
